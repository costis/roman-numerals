Roman to Decimal convertor
==========================
Ruby code to convert Roman Numerals to their decimal integer equivalent. 

Install
-------
`bundle install`

Run tests
---------
`rspec spec`
