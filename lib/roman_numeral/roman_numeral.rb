class RomanNumeral
  ROMAN_DECIMAL_MAP = {"M" => 1000, "D" => 500, "C" => 100, "L" => 50, "X" => 10, "V" => 5, "I" => 1}

  def initialize(input)
    @input = input
  end

  def to_integer
    arr = @input.each_char.map {|c| ROMAN_DECIMAL_MAP[c]}
    res = 0

    arr.each_with_index do |num, idx| 
      if(idx < arr.size - 1 and num < arr[idx + 1])
        res += arr[idx + 1] - num
        arr.delete_at(idx + 1)
       next
      else
        res += num
      end
    end

    res 
  end

end
