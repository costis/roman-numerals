require 'spec_helper'

describe RomanNumeral do
  describe "#to_integer" do

    it "should return the correct number - case 1" do
      r = RomanNumeral.new("MCM")
      r.to_integer.should eq 1900
    end

    it "should return the correct number - case 2" do
      r = RomanNumeral.new("LVII")
      r.to_integer.should eq 57
    end

    it "should return the correct number - case 3" do
      r = RomanNumeral.new("MCMXCVIII")
      r.to_integer.should eq 1998 
    end

    it "should return the correct number - case 4" do
      r = RomanNumeral.new("I")
      r.to_integer.should eq 1
    end

    it "should return the correct number - case 5" do
      r = RomanNumeral.new("MMMCMXCIX")
      r.to_integer.should eq 3999 
    end

  end

end
